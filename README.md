## Coin-calendar

![Check it out](/assets/img/preview.png)

[DEMO](https://natizyskunk.gitlab.io/coin-calendar/)

### Infos
- You can use the [editor](https://natizyskunk.gitlab.io/coin-calendar/edit/master/README.md) on this Gitlab repo to maintain the README markdown file.

- Whenever you commit to this repository, Gitlab Pages will run [Jekyll](https://jekyllrb.com) to rebuild the pages in your site, from the content in your HTML/CSS files.

### Support or Contact

- Having trouble with Pages? Check out our [documentation](https://gitlab.com/help/user/project/pages/index.md) or [contact support](https://about.gitlab.com/support) and we’ll help you sort it out.